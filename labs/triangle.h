#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <string>
using namespace std;

#include "shape.h"

class triangle: public shape {
public:
triangle(double width, double height, string colour);
double area();


};

#endif