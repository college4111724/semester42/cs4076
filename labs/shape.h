#ifndef SHAPE_H
#define SHAPE_H

#include <string>
using namespace std;

class shape 
{
    
 
    public:
     double width;
    double height;
    string colour;
    shape(double width, double height, string colour);
    virtual double area() = 0;
    void displayColour();

     
};


#endif