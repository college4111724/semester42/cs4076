#include <iostream>
#include "shape.h"
#include "triangle.h"
#include "rectangle.h"

int main() {
    triangle t(3, 3, "red");
    rectangle r(5,5,"green");
    cout << "Triangle area: " << t.area() << endl;
    t.displayColour();
    cout << "Rectangle area: " << r.area() << endl;
    return 0;
}