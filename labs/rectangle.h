#ifndef RECTANGLE_H
#define RECTANGLE_H
#include <string>

#include "shape.h"

class rectangle: public shape {
   public:
    rectangle (double width, double height, string colour);
    double area();

};

#endif